package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        plain1 = findViewById(R.id.plain1);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text5 = findViewById(R.id.text5);
        text6 = findViewById(R.id.text6);
        //test

    }
    EditText plain1;
    TextView text1;
    TextView text2;
    TextView text3;
    TextView text5;
    TextView text6;



    public  void City(){
        String url = "https://api.openweathermap.org/data/2.5/weather?q=" + plain1.getText().toString() + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric";
        JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject main = response.getJSONObject("main");
                    String temp = String.valueOf(main.getInt("temp"));
                    text1.setText(temp.toString());
                    String feel = String.valueOf(main.getInt("feels_like"));
                    text2.setText("Ощущается как: "+feel.toString());
                    JSONObject wind = response.getJSONObject("wind");
                    String speed = String.valueOf(wind.getInt("speed"));
                    text3.setText("Скорость ветра: "+speed.toString());
                    JSONObject coord = response.getJSONObject("coord");
                    String lat = String.valueOf(coord.getInt("lat"));
                    text5.setText("Широта: "+lat.toString());
                    String lon = String.valueOf(coord.getInt("lon"));
                    text6.setText("Долгота: "+lon.toString());
                } catch (JSONException e) {text1.setText(e.getMessage()); } }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { }});
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jo);
    }



    public void clickWeather(View view) {
        City();
    }
}
